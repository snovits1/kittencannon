Super Cannon for iOS by Steven Novitske

Compatible with all iOS devices.

A game just like Kitten Cannon. Orange blocks (TNT) double the ball's horizontal and vertical velocity. Red blocks (trampoline) double the ball's vertical velocity. Both gray and black blocks (spikes and flytrap) stop the ball on impact. Change the angle of the cannon to anywhere between 15 and 75 degrees, try to launch it when the power meter is at its highest, and let luck do the rest.