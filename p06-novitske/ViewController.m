//
//  ViewController.m
//  p06-novitske
//
//  Created by Steven Novitske on 4/6/17.
//  Copyright © 2017 Steven Novitske. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#define toRadians *0.0174533
#define numObstacles 10
#define spacing (rand()%201 + 300) //Random number between 300 and 500 to space obstacles
#define randSize (rand()%41 + 60) //Random number between 60 and 100 to size obstacles

@interface ViewController ()

@end

int screenWidth;
int screenHeight;
CADisplayLink* display;
UIButton* launch;
UIImageView* clouds1;
UIImageView* clouds2;
UIImageView* grass1;
UIImageView* grass2;
UIView* powerMeter;
UIView* bar;
int barChange;
float power;
UIView* obj;
float dx;
float dy;
UIStepper* angleChanger;
UILabel* angleLabel;
UILabel* heightLabel;
UIButton* newGameButton;
UILabel* scoreLabel;
int score;
UIView* cannon;
NSMutableArray* obstacles;
int highScore;
UILabel* highScoreLabel;
bool gameOver;

AVAudioPlayer *boom;
AVAudioPlayer *bounce;
AVAudioPlayer *boing;
AVAudioPlayer *thump;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    screenWidth = self.view.bounds.size.width;
    screenHeight = self.view.bounds.size.height;
    power = 0;
    
    //Set up display link
    display = [CADisplayLink displayLinkWithTarget:self selector:@selector(mainRoutine:)];
    display.preferredFramesPerSecond = 60;
    [display addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    //Set up launch button
    launch = [[UIButton alloc] initWithFrame:CGRectMake(screenWidth*0.3, screenHeight*0.3, screenWidth*0.4, screenHeight*0.2)];
    [launch setTitle:@"LAUNCH" forState:UIControlStateNormal];
    [launch setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    launch.titleLabel.font = [UIFont boldSystemFontOfSize:launch.frame.size.height/2];
    launch.backgroundColor = [UIColor redColor];
    launch.layer.cornerRadius = 10;
    launch.layer.masksToBounds = YES;
    [launch addTarget:self action:@selector(launchPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    //Set up background images
    clouds1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    clouds2 = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth, 0, screenWidth, screenHeight)];
    grass1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    grass2 = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth, 0, screenWidth, screenHeight)];
    clouds1.image = [UIImage imageNamed:@"clouds.png"];
    clouds2.image = [UIImage imageNamed:@"clouds.png"];
    grass1.image = [UIImage imageNamed:@"grass.png"];
    grass2.image = [UIImage imageNamed:@"grass.png"];
    
    //Set up object
    obj = [[UIView alloc] initWithFrame:CGRectMake(100, 200, 30, 30)];
    obj.backgroundColor = [UIColor yellowColor];
    obj.layer.cornerRadius = 15;
    obj.layer.masksToBounds = YES;
    [obj setHidden:YES];
    dx = 0;
    dy = 0;
    
    //Set up angle label and stepper
    angleChanger = [[UIStepper alloc] initWithFrame:CGRectMake(20, screenHeight-49, 94, 29)];
    angleChanger.minimumValue = 15;
    angleChanger.maximumValue = 75;
    angleChanger.value = 45;
    angleChanger.wraps = YES;
    angleChanger.tintColor = [UIColor blackColor];
    angleChanger.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    angleChanger.layer.cornerRadius = 5;
    angleChanger.layer.masksToBounds = YES;
    angleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, angleChanger.frame.origin.y-20, 94, 15)];
    angleLabel.layer.cornerRadius = 5;
    angleLabel.layer.masksToBounds = YES;
    angleLabel.text = [NSString stringWithFormat:@"%.0lf°",angleChanger.value];
    angleLabel.textAlignment = NSTextAlignmentCenter;
    angleLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    
    //Set up power meter
    powerMeter = [[UIView alloc] initWithFrame:CGRectMake(10, (angleLabel.frame.origin.y-25)/2, 93, 20)];
    CAGradientLayer* gradient = [CAGradientLayer layer];
    gradient.frame = powerMeter.frame;
    gradient.colors = @[(id)[UIColor yellowColor].CGColor, (id)[UIColor redColor].CGColor];
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    [powerMeter.layer insertSublayer:gradient atIndex:10];
    bar = [[UIView alloc] initWithFrame:powerMeter.frame];
    bar.backgroundColor = [UIColor blackColor];
    [powerMeter addSubview:bar];
    
    //Set up height-above-screen label
    heightLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 90, 15)];
    heightLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    heightLabel.layer.cornerRadius = 5;
    heightLabel.layer.masksToBounds = YES;
    heightLabel.textAlignment = NSTextAlignmentCenter;
    [heightLabel setHidden:YES];
    
    //Set up restart button
    newGameButton = [[UIButton alloc] initWithFrame:launch.frame];
    [newGameButton setTitle:@"New Game" forState:UIControlStateNormal];
    [newGameButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    newGameButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    newGameButton.layer.cornerRadius = 10;
    newGameButton.layer.masksToBounds = YES;
    newGameButton.titleLabel.font = [UIFont boldSystemFontOfSize:newGameButton.frame.size.height/2];
    [newGameButton setHidden:YES];
    [newGameButton addTarget:self action:@selector(newGame:) forControlEvents:UIControlEventTouchUpInside];
    
    //Set up score label
    score = 0;
    scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 120, 30)];
    scoreLabel.text = [NSString stringWithFormat:@"Score: %d ft", score];
    scoreLabel.textColor = [UIColor orangeColor];
    scoreLabel.font = [UIFont boldSystemFontOfSize:16];
    
    //Set up cannon
    cannon = [[UIView alloc] initWithFrame:CGRectMake(20, screenHeight*0.7, 200, 40)];
    cannon.backgroundColor = [UIColor grayColor];
    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, -45 toRadians);
    cannon.transform = transform;
    obj.center = cannon.center;
    
    //Set up obstacles
    UIView* obstacle = [[UIView alloc] initWithFrame:CGRectMake(screenWidth, screenHeight*0.75, randSize, randSize)];
    obstacle.center = CGPointMake(obstacle.center.x, screenHeight*0.75);
    obstacles = [NSMutableArray arrayWithObject:obstacle];
    for(int i=0; i<numObstacles-1; i++) {
        UIView* prev = obstacles[i];
        UIView* next = [[UIView alloc] initWithFrame:CGRectMake(prev.frame.origin.x + spacing, screenHeight*0.75, randSize, randSize)];
        next.center = CGPointMake(next.center.x, screenHeight*0.75);
        int chance = rand()%10000;
        if(chance >= 0 && chance < 2500) next.backgroundColor = [UIColor orangeColor];//Doubles dx and dy
        else if(chance >= 2500 && chance < 5000) next.backgroundColor = [UIColor redColor];//Doubles dy
        else if(chance >= 5000 && chance < 7500) next.backgroundColor = [UIColor darkGrayColor];//Kills object
        else if(chance >= 7500 && chance < 10000) next.backgroundColor = [UIColor blackColor];//Kills object
        [obstacles addObject:next];
    }
    
    //Set up high score label
    highScore = 0;
    highScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth*0.3, screenHeight*0.1, screenWidth*0.4, screenHeight*0.1)];
    highScoreLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    highScoreLabel.font = [UIFont boldSystemFontOfSize:highScoreLabel.frame.size.height/2];
    highScoreLabel.textAlignment = NSTextAlignmentCenter;
    highScoreLabel.layer.cornerRadius = 10;
    highScoreLabel.layer.masksToBounds = YES;
    [highScoreLabel setHidden:YES];
    gameOver = NO;

    //Set up sounds
    NSString *path = [NSString stringWithFormat:@"%@/Daktak-Drop.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    boom = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/Twang1.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    bounce = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/spring.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    boing = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    path = [NSString stringWithFormat:@"%@/PUNCH1.wav", [[NSBundle mainBundle] resourcePath]];
    soundUrl = [NSURL fileURLWithPath:path];
    thump = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    
    //Add subviews
    [self.view addSubview:clouds1];
    [self.view addSubview:clouds2];
    [self.view addSubview:grass1];
    [self.view addSubview:grass2];
    [self.view addSubview:launch];
    [self.view addSubview:cannon];
    [self.view addSubview:powerMeter];
    [self.view addSubview:angleChanger];
    [self.view addSubview:angleLabel];
    [self.view addSubview:heightLabel];
    [self.view addSubview:newGameButton];
    [self.view addSubview:scoreLabel];
    for(int i=0; i<numObstacles; i++) {
        UIView* obs = obstacles[i];
        [self.view addSubview:obs];
    }
    [self.view addSubview:obj];
    [self.view addSubview:highScoreLabel];
}

- (void)mainRoutine:(CADisplayLink*)sender {
    if(launch.isHidden) [self moveObjects];
    if(bar.bounds.size.width == 0) barChange = 3;
    else if(bar.bounds.size.width == powerMeter.bounds.size.width) barChange = -3;
    bar.frame = CGRectMake(bar.frame.origin.x - barChange, bar.frame.origin.y, bar.frame.size.width + barChange, bar.frame.size.height);
    angleLabel.text = [NSString stringWithFormat:@"%.0lf°",angleChanger.value];
    scoreLabel.text = [NSString stringWithFormat:@"Score: %d ft", score/100];
    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, -(angleChanger.value) toRadians);
    cannon.transform = transform;
}

- (IBAction)launchPressed:(UIButton*)sender {
    

    [boom play];
    
    power = (powerMeter.bounds.size.width - bar.bounds.size.width)*0.9;
    [launch setHidden:YES];
    [powerMeter setHidden:YES];
    [angleLabel setHidden:YES];
    [angleChanger setHidden:YES];
    [cannon setHidden:YES];
    [obj setHidden:NO];
    dx = power*cos(angleChanger.value toRadians);
    dy = power*sin(angleChanger.value toRadians);
}

- (void)moveObjects {
    score += dx;
    dy -= .6;
    //Check if object hits obstacle and react accordingly
    bool hit = NO;
    if(obj.center.y > 0 && dy < 0) {
        for(int i=0; i<numObstacles; i++) {
            UIView* obs = obstacles[i];
            if(CGRectIntersectsRect(obs.frame, obj.frame)) {
                hit = YES;
                if(obs.backgroundColor == [UIColor orangeColor]) {
                    if(!gameOver)[boom play];
                    dx *= 2;
                    dy *= -2;
                    if(dy > -10 && dx < 2) dy = -10;
                } else if(obs.backgroundColor == [UIColor redColor]) {
                    if(!gameOver)[boing play];
                    dy *= -2;
                } else if(obs.backgroundColor == [UIColor darkGrayColor] || obs.backgroundColor == [UIColor blackColor]) {
                    if(!gameOver)[thump play];
                    dx = 0;
                    dy = 0;
                }
            }
        }
    }
    //Bounce object on ground if no obstacle was hit
    if(!hit && dy < 0 && obj.center.y >= screenHeight*0.8) {
        if(!gameOver)[bounce play];
        dx *= 0.75;
        dy *= -0.75;
        if(dx <= 1) {
            dx = 0;
            dy = 0;
        }
    }
    //Display label for height-above-screen
    if(obj.center.y < 0) {
        int height = -(obj.center.y)/100;
        heightLabel.text = [NSString stringWithFormat:@"%d ft", height];
        heightLabel.center = CGPointMake(obj.center.x, 30);
        [heightLabel setHidden:NO];
    } else {
        [heightLabel setHidden:YES];
    }
    //Move background and obstacles once object has traveled halfway across screen
    if(obj.center.x >= screenWidth/2) {
        //Object
        obj.center = CGPointMake(obj.center.x, obj.center.y - dy);
        //Background
        clouds1.center = CGPointMake(clouds1.center.x - dx/6, clouds1.center.y);
        clouds2.center = CGPointMake(clouds2.center.x - dx/6, clouds2.center.y);
        grass1.center = CGPointMake(grass1.center.x - dx, grass1.center.y);
        grass2.center = CGPointMake(grass2.center.x - dx, grass2.center.y);
        if(clouds1.frame.origin.x < -screenWidth) clouds1.frame = CGRectMake(clouds2.frame.origin.x + screenWidth, 0, screenWidth, screenHeight);
        if(clouds2.frame.origin.x < -screenWidth) clouds2.frame = CGRectMake(clouds1.frame.origin.x + screenWidth, 0, screenWidth, screenHeight);
        if(grass1.frame.origin.x < -screenWidth) grass1.frame = CGRectMake(grass2.frame.origin.x + screenWidth, 0, screenWidth, screenHeight);
        if(grass2.frame.origin.x < -screenWidth) grass2.frame = CGRectMake(grass1.frame.origin.x + screenWidth, 0, screenWidth, screenHeight);
        //Obstacles
        for(int i=0; i<numObstacles; i++) {
            UIView* obs = obstacles[i];
            obs.center = CGPointMake(obs.center.x - dx, obs.center.y);
            if(obs.frame.origin.x < -obs.frame.size.width) {
                UIView* prev;
                if(i==0) prev = obstacles[numObstacles-1];
                else prev = obstacles[i-1];
                obs.frame = CGRectMake(prev.frame.origin.x + spacing, screenHeight*0.75, randSize, randSize);
                obs.center = CGPointMake(obs.center.x, screenHeight*0.75);
                int chance = rand()%10000;
                if(chance >= 0 && chance < 2500) obs.backgroundColor = [UIColor orangeColor];//Doubles dx and dy
                else if(chance >= 2500 && chance < 5000) obs.backgroundColor = [UIColor redColor];//Doubles dy
                else if(chance >= 5000 && chance < 7500) obs.backgroundColor = [UIColor darkGrayColor];//Kills object
                else if(chance >= 7500 && chance < 10000) obs.backgroundColor = [UIColor blackColor];//Kills object
            }
        }
    } else {
        obj.center = CGPointMake(obj.center.x + dx, obj.center.y - dy);
    }
    //Game over
    if(dx == 0 && !gameOver) {
        gameOver = YES;
        [newGameButton setHidden:NO];
        [highScoreLabel setHidden:NO];
        if(score > highScore) {
            highScore = score;
            highScoreLabel.text = [NSString stringWithFormat:@"New High Score!"];//, highScore/100];
            highScoreLabel.textColor = [UIColor greenColor];
        } else {
            highScoreLabel.text = [NSString stringWithFormat:@"High score: %d ft", highScore/100];
            highScoreLabel.textColor = [UIColor orangeColor];
        }
    }
}

- (IBAction)newGame:(UIButton*)sender {
    power = 0;
    dx = 0;
    dy = 0;
    score = 0;
    [launch setHidden:NO];
    [powerMeter setHidden:NO];
    [angleLabel setHidden:NO];
    [angleChanger setHidden:NO];
    [cannon setHidden:NO];
    [obj setHidden:YES];
    [newGameButton setHidden:YES];
    [highScoreLabel setHidden:YES];
    gameOver = NO;
    obj.center = cannon.center;
    UIView* obstacle = obstacles[0];
    obstacle.frame = CGRectMake(screenWidth, screenHeight*0.75, randSize, randSize);
    obstacle.center = CGPointMake(obstacle.center.x, screenHeight*0.75);
    for(int i=1; i<numObstacles; i++) {
        UIView *prev = obstacles[i-1];
        UIView *next = obstacles[i];
        next.frame = CGRectMake(prev.frame.origin.x + spacing, screenHeight*0.75, randSize, randSize);
        next.center = CGPointMake(next.center.x, screenHeight*0.75);
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
